package com.websystique.springmvc.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the eventpicture database table.
 * 
 */
@Entity
@Table(name="eventpicture")
@NamedQuery(name="Eventpicture.findAll", query="SELECT e FROM Eventpicture e")
public class Eventpicture implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false)
	private int idEventPicture;

	@Temporal(TemporalType.TIMESTAMP)
	private Date dateSeted;

	//bi-directional many-to-one association to Event
	@ManyToOne
	@JoinColumn(name="idEvent", nullable=false)
	private Event event;

	//bi-directional many-to-one association to Picture
	@ManyToOne
	@JoinColumn(name="idPicture", nullable=false)
	private Picture picture;

	public Eventpicture() {
	}

	public int getIdEventPicture() {
		return this.idEventPicture;
	}

	public void setIdEventPicture(int idEventPicture) {
		this.idEventPicture = idEventPicture;
	}

	public Date getDateSeted() {
		return this.dateSeted;
	}

	public void setDateSeted(Date dateSeted) {
		this.dateSeted = dateSeted;
	}

	public Event getEvent() {
		return this.event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

	public Picture getPicture() {
		return this.picture;
	}

	public void setPicture(Picture picture) {
		this.picture = picture;
	}

}