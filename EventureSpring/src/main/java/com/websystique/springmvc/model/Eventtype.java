package com.websystique.springmvc.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the eventtype database table.
 * 
 */
@Entity
@Table(name="eventtype")
@NamedQuery(name="Eventtype.findAll", query="SELECT e FROM Eventtype e")
public class Eventtype implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false)
	private int idEventType;

	@Column(nullable=false, length=500)
	private String description;

	@Column(nullable=false, length=255)
	private String etypeName;

	//bi-directional many-to-one association to Event
	@OneToMany(fetch = FetchType.LAZY,mappedBy="eventtype")
	private List<Event> events;

	//bi-directional many-to-one association to Userevent
	@OneToMany(fetch = FetchType.LAZY,mappedBy="eventtype")
	private List<Userevent> userevents;

	public Eventtype() {
	}

	public int getIdEventType() {
		return this.idEventType;
	}

	public void setIdEventType(int idEventType) {
		this.idEventType = idEventType;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getEtypeName() {
		return this.etypeName;
	}

	public void setEtypeName(String etypeName) {
		this.etypeName = etypeName;
	}

	public List<Event> getEvents() {
		return this.events;
	}

	public void setEvents(List<Event> events) {
		this.events = events;
	}

	public Event addEvent(Event event) {
		getEvents().add(event);
		event.setEventtype(this);

		return event;
	}

	public Event removeEvent(Event event) {
		getEvents().remove(event);
		event.setEventtype(null);

		return event;
	}

	public List<Userevent> getUserevents() {
		return this.userevents;
	}

	public void setUserevents(List<Userevent> userevents) {
		this.userevents = userevents;
	}

	public Userevent addUserevent(Userevent userevent) {
		getUserevents().add(userevent);
		userevent.setEventtype(this);

		return userevent;
	}

	public Userevent removeUserevent(Userevent userevent) {
		getUserevents().remove(userevent);
		userevent.setEventtype(null);

		return userevent;
	}

}