package com.websystique.springmvc.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the follow database table.
 * 
 */
@Entity
@Table(name="follow")
@NamedQuery(name="Follow.findAll", query="SELECT f FROM Follow f")
public class Follow implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false)
	private int idFollow;

	@Column(length=1256)
	private String comment;

	//bi-directional many-to-one association to Event
	@ManyToOne
	@JoinColumn(name="idEvent", nullable=false)
	private Event event;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name="idUser", nullable=false)
	private User user;

	public Follow() {
	}

	public int getIdFollow() {
		return this.idFollow;
	}

	public void setIdFollow(int idFollow) {
		this.idFollow = idFollow;
	}

	public String getComment() {
		return this.comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Event getEvent() {
		return this.event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}