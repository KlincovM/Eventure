package com.websystique.springmvc.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the user database table.
 * 
 */
@Entity
@Table(name="user")
@NamedQuery(name="User.findAll", query="SELECT u FROM User u")
public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false)
	private int idUser;

	@Temporal(TemporalType.TIMESTAMP)
	private Date birthDate;

	@Column(nullable=false, length=255)
	private String firstName;

	@Column(nullable=false)
	private byte genre;

	@Column(nullable=false, length=255)
	private String lastName;

	//bi-directional many-to-one association to Follow
	@OneToMany(fetch = FetchType.LAZY,mappedBy="user")
	private List<Follow> follows;

	//bi-directional many-to-one association to Userevent
	@OneToMany(fetch = FetchType.LAZY,mappedBy="user")
	private List<Userevent> userevents;

	public User() {
	}

	public int getIdUser() {
		return this.idUser;
	}

	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}

	public Date getBirthDate() {
		return this.birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public byte getGenre() {
		return this.genre;
	}

	public void setGenre(byte genre) {
		this.genre = genre;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public List<Follow> getFollows() {
		return this.follows;
	}

	public void setFollows(List<Follow> follows) {
		this.follows = follows;
	}

	public Follow addFollow(Follow follow) {
		getFollows().add(follow);
		follow.setUser(this);

		return follow;
	}

	public Follow removeFollow(Follow follow) {
		getFollows().remove(follow);
		follow.setUser(null);

		return follow;
	}

	public List<Userevent> getUserevents() {
		return this.userevents;
	}

	public void setUserevents(List<Userevent> userevents) {
		this.userevents = userevents;
	}

	public Userevent addUserevent(Userevent userevent) {
		getUserevents().add(userevent);
		userevent.setUser(this);

		return userevent;
	}

	public Userevent removeUserevent(Userevent userevent) {
		getUserevents().remove(userevent);
		userevent.setUser(null);

		return userevent;
	}

}