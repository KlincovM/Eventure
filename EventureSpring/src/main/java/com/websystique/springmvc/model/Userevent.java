package com.websystique.springmvc.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the userevent database table.
 * 
 */
@Entity
@Table(name="userevent")
@NamedQuery(name="Userevent.findAll", query="SELECT u FROM Userevent u")
public class Userevent implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false)
	private int idUserEvent;

	@Temporal(TemporalType.TIMESTAMP)
	private Date dateSeted;

	//bi-directional many-to-one association to Eventtype
	@ManyToOne
	@JoinColumn(name="idEventType", nullable=false)
	private Eventtype eventtype;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name="idUser", nullable=false)
	private User user;

	public Userevent() {
	}

	public int getIdUserEvent() {
		return this.idUserEvent;
	}

	public void setIdUserEvent(int idUserEvent) {
		this.idUserEvent = idUserEvent;
	}

	public Date getDateSeted() {
		return this.dateSeted;
	}

	public void setDateSeted(Date dateSeted) {
		this.dateSeted = dateSeted;
	}

	public Eventtype getEventtype() {
		return this.eventtype;
	}

	public void setEventtype(Eventtype eventtype) {
		this.eventtype = eventtype;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}