package com.websystique.springmvc.model;

import java.io.Serializable;
import javax.persistence.*;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.sql.Time;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the event database table.
 * 
 */
@Entity
@Table(name="event")
@NamedQuery(name="Event.findAll", query="SELECT e FROM Event e")
public class Event implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false)
	private int idEvent;

	@Temporal(TemporalType.DATE)
	private Date dateToStart;

	private int foolowers;

	@Column(nullable=false, length=20)
	private String latitude;

	@Column(length=2000)
	private String longDescription;

	@Column(nullable=false, length=20)
	private String longitude;

	@Column(nullable=false, length=255)
	private String nameEvent;

	@Column(length=255)
	private String shortDescription;

	@Column(nullable=false)
	private Time timeToStart;

	//bi-directional many-to-one association to City
	@ManyToOne
	@JoinColumn(name="idCity", nullable=false)
	private City city;

	//bi-directional many-to-one association to Eventtype
	@ManyToOne
	@JoinColumn(name="idEventType", nullable=false)
	private Eventtype eventtype;

	//bi-directional many-to-one association to Organization
	@ManyToOne
	@JoinColumn(name="idOrganization", nullable=false)
	private Organization organization;

	//bi-directional many-to-one association to Eventpicture
	@OneToMany(fetch = FetchType.LAZY,mappedBy="event")
	private List<Eventpicture> eventpictures;

	//bi-directional many-to-one association to Follow
	@OneToMany(fetch = FetchType.LAZY,mappedBy="event")
	private List<Follow> follows;

	public Event() {
	}

	public int getIdEvent() {
		return this.idEvent;
	}

	public void setIdEvent(int idEvent) {
		this.idEvent = idEvent;
	}

	public Date getDateToStart() {
		return this.dateToStart;
	}

	public void setDateToStart(Date dateToStart) {
		this.dateToStart = dateToStart;
	}

	public int getFoolowers() {
		return this.foolowers;
	}

	public void setFoolowers(int foolowers) {
		this.foolowers = foolowers;
	}

	public String getLatitude() {
		return this.latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongDescription() {
		return this.longDescription;
	}

	public void setLongDescription(String longDescription) {
		this.longDescription = longDescription;
	}

	public String getLongitude() {
		return this.longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getNameEvent() {
		return this.nameEvent;
	}

	public void setNameEvent(String nameEvent) {
		this.nameEvent = nameEvent;
	}

	public String getShortDescription() {
		return this.shortDescription;
	}

	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	public Time getTimeToStart() {
		return this.timeToStart;
	}

	public void setTimeToStart(Time timeToStart) {
		this.timeToStart = timeToStart;
	}

	public City getCity() {
		return this.city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	public Eventtype getEventtype() {
		return this.eventtype;
	}

	public void setEventtype(Eventtype eventtype) {
		this.eventtype = eventtype;
	}

	public Organization getOrganization() {
		return this.organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public List<Eventpicture> getEventpictures() {
		return this.eventpictures;
	}

	public void setEventpictures(List<Eventpicture> eventpictures) {
		this.eventpictures = eventpictures;
	}

	public Eventpicture addEventpicture(Eventpicture eventpicture) {
		getEventpictures().add(eventpicture);
		eventpicture.setEvent(this);

		return eventpicture;
	}

	public Eventpicture removeEventpicture(Eventpicture eventpicture) {
		getEventpictures().remove(eventpicture);
		eventpicture.setEvent(null);

		return eventpicture;
	}

	public List<Follow> getFollows() {
		return this.follows;
	}

	public void setFollows(List<Follow> follows) {
		this.follows = follows;
	}

	public Follow addFollow(Follow follow) {
		getFollows().add(follow);
		follow.setEvent(this);

		return follow;
	}

	public Follow removeFollow(Follow follow) {
		getFollows().remove(follow);
		follow.setEvent(null);

		return follow;
	}

}