package com.websystique.springmvc.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the picture database table.
 * 
 */
@Entity
@Table(name="picture")
@NamedQuery(name="Picture.findAll", query="SELECT p FROM Picture p")
public class Picture implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false)
	private int idPicture;

	@Lob
	@Column(nullable=false)
	private byte[] image;

	@Column(length=255)
	private String imageName;

	@Column(length=45)
	private String imageSize;

	@Column(length=45)
	private String imagetype;

	private int important;

	//bi-directional many-to-one association to Eventpicture
	@OneToMany(fetch = FetchType.LAZY,mappedBy="picture")
	private List<Eventpicture> eventpictures;

	public Picture() {
	}

	public int getIdPicture() {
		return this.idPicture;
	}

	public void setIdPicture(int idPicture) {
		this.idPicture = idPicture;
	}

	public byte[] getImage() {
		return this.image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public String getImageName() {
		return this.imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public String getImageSize() {
		return this.imageSize;
	}

	public void setImageSize(String imageSize) {
		this.imageSize = imageSize;
	}

	public String getImagetype() {
		return this.imagetype;
	}

	public void setImagetype(String imagetype) {
		this.imagetype = imagetype;
	}

	public int getImportant() {
		return this.important;
	}

	public void setImportant(int important) {
		this.important = important;
	}

	public List<Eventpicture> getEventpictures() {
		return this.eventpictures;
	}

	public void setEventpictures(List<Eventpicture> eventpictures) {
		this.eventpictures = eventpictures;
	}

	public Eventpicture addEventpicture(Eventpicture eventpicture) {
		getEventpictures().add(eventpicture);
		eventpicture.setPicture(this);

		return eventpicture;
	}

	public Eventpicture removeEventpicture(Eventpicture eventpicture) {
		getEventpictures().remove(eventpicture);
		eventpicture.setPicture(null);

		return eventpicture;
	}

}