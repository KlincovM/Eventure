package com.websystique.springmvc.service;

import java.util.List;

import com.websystique.springmvc.model.Employee;
import com.websystique.springmvc.model.Event;

public interface EventService {
	
	List<Event> findAll();
}
