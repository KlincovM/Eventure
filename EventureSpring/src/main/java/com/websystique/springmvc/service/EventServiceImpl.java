package com.websystique.springmvc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.websystique.springmvc.dao.EventDao;
import com.websystique.springmvc.model.Event;

@Service("eventService")
@Transactional
public class EventServiceImpl implements EventService{

	@Autowired
	EventDao eventDao;

	public List<Event> findAll() {
		// TODO Auto-generated method stub
		return eventDao.findAllEvents();
	}
	
}
