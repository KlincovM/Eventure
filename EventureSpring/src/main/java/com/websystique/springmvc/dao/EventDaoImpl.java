package com.websystique.springmvc.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.websystique.springmvc.model.Employee;
import com.websystique.springmvc.model.Event;

@Repository("eventDao")
public class EventDaoImpl extends AbstractDao<Integer, Event> implements EventDao {

	public List<Event> findAllEvents() {
		// TODO Auto-generated method stub
		Criteria criteria = createEntityCriteria();
		//criteria.add(Restrictions.eq("ssn", ssn));
		return criteria.list();
		
	}

	

}
