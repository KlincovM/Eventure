package com.websystique.springmvc.dao;

import java.util.List;

import com.websystique.springmvc.model.Employee;
import com.websystique.springmvc.model.Event;

public interface EventDao {

	List<Event> findAllEvents();

	
}
