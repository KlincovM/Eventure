package com.websystique.springmvc.controller;

import java.util.List;

import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.util.JSONPObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.websystique.springmvc.jsonmodel.EventJson;
import com.websystique.springmvc.model.Employee;
import com.websystique.springmvc.model.Event;
import com.websystique.springmvc.render.EventRender;
import com.websystique.springmvc.service.EmployeeService;
import com.websystique.springmvc.service.EventService;
import com.websystique.springmvc.service.EventServiceImpl;

@RestController
@RequestMapping("/event")
public class EventController {

	@Autowired
	EventService service;
	
	@RequestMapping(value = { "/", "/list" },produces = "application/json")
	public ResponseEntity<?> listEmployees() {
		List<Event> events = service.findAll();
		//model.addAttribute("events", events);
		EventRender eventRender=new EventRender();
		EventJson eventJ=eventRender.modelToJson(events.get(0));
		return new ResponseEntity(eventJ, HttpStatus.OK);
	}
}
